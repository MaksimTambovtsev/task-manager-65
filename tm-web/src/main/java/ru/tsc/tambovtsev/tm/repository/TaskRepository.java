package ru.tsc.tambovtsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        write(new Task("Task 1"));
        write(new Task("Task 2"));
        write(new Task("Task 3"));
    }

    public Task write(@NotNull final Task task) {
        return tasks.put(task.getId(), task);
    }

    public void create() {
        write(new Task("New Task" + System.currentTimeMillis()));
    }

    public void save(@NotNull final Task task) {
        tasks.replace(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void remove(@NotNull final Task task) {
        tasks.remove(task.getId());
    }

    public void remove(@NotNull final List<Task> tasks) {
        tasks
                .stream()
                .forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return tasks.containsKey(id);
    }

    public void clear() {
        tasks.clear();
    }

    public long count() {
        return tasks.size();
    }

}
