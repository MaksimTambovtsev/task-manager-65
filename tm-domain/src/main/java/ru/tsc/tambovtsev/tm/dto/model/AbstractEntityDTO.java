package ru.tsc.tambovtsev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractEntityDTO implements Serializable {

    @Id
    @Column(name = "ID")
    private String id = UUID.randomUUID().toString();

}
