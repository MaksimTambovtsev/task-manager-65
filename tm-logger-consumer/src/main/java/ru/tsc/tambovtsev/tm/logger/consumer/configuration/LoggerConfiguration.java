package ru.tsc.tambovtsev.tm.logger.consumer.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.tambovtsev.tm.logger.consumer.listener.EntityListener;
import ru.tsc.tambovtsev.tm.logger.consumer.service.LogService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.tsc.tambovtsev.tm.logger.consumer")
public class LoggerConfiguration {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        return factory;
    }

    @Bean
    public EntityListener listener() {
        @NotNull final EntityListener entityListener = new EntityListener();
        return entityListener;
    }

}
