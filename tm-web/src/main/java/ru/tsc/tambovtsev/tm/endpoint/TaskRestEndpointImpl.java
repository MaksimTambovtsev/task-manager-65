package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.tambovtsev.tm.api.TaskRestEndpoint;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.repository.TaskRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskRepository
                .findAll()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    @PostMapping("/save")
    public Task save(@NotNull @RequestBody final Task task) {
        return taskRepository.write(task);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Task task) {
        taskRepository.remove(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<Task> tasks) {
        taskRepository.remove(tasks);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        taskRepository.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        taskRepository.removeById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }
    
}
