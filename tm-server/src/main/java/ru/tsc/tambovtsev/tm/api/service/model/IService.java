package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractEntity> {

    @Nullable
    List<M> findAll();

    void addAll(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void create(@Nullable M model);

}

