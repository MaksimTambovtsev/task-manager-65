package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.SortTable;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable SortTable sortTable);

    long getSize(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

}
