package ru.tsc.tambovtsev.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.tambovtsev.tm")
public class ApplicationConfiguration {
}
